package shenjie.wifi.wechat;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String args[]) {
        SpringApplication.run(Application.class, args);
    }

    private static final String insertSQL = "INSERT INTO PERFORMANCE_TEST(ID, COLUMN1, COLUMN2) VALUES (?,?,?)";
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    InsertDao insertDao;

    @Autowired
    DataSource dataSource;

    @Override
    public void run(String... strings) throws Exception {


        log.info("Creating tables");

        try {
            jdbcTemplate.execute("DROP TABLE PERFORMANCE_TEST");
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        jdbcTemplate.execute("CREATE TABLE PERFORMANCE_TEST(ID int, COLUMN1 nvarchar(255), COLUMN2 nvarchar(255))");
        log.info("tables created");


        List<Object[]> works = DataGeneraterHelper.generateMockData();

        long start = System.currentTimeMillis();
        insertDao.doSimpleJdbcInsert(works);
//        insertDao.doInPlainJdbc(works);

        long end = System.currentTimeMillis();
        long total = (end - start) / 1000 ;
        log.info("time is :" + total);
        log.info("Querying for wrk_in_out_record records ");

    }

}